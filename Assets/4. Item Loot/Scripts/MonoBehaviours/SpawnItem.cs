﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItem : MonoBehaviour, ISpawns
{
    public ItemPickUps_SO[] itemDefinitions;

    private int _whichToSpawn = 0;
    private int _totalSpawnWeight = 0;
    private int _chosen = 0;

    public Rigidbody ItemSpawned { get; set; }
    public Renderer ItemMaterial { get; set; }
    public ItemPickUp ItemType { get; set; }

    private void Start()
    {
        foreach (ItemPickUps_SO itemPick in itemDefinitions)
        {
            _totalSpawnWeight += itemPick.spawnChanceWeight;
        }
    }

    public void CreateSpawn()
    {
        foreach (ItemPickUps_SO itemPick in itemDefinitions)
        {
            _whichToSpawn += itemPick.spawnChanceWeight;
            if (_whichToSpawn >= _chosen)
            {
                ItemSpawned = Instantiate(itemPick.itemSpawnObject,
                    transform.position, Quaternion.identity);

                ItemMaterial = ItemSpawned.GetComponent<Renderer>();
                ItemMaterial.material = itemPick.itemMaterial;

                ItemType = ItemSpawned.GetComponent<ItemPickUp>();
                ItemType.itemDefinition = itemPick;
                break;
            }
        }
    }
}
